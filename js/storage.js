jsonmovies = {
    batman:{
        director:"llllll",
        year: 1989
    },
    batman2:{
        director:"aaaaaa"
    },
};

localStorage.setItem("movies", JSON.stringify(jsonmovies));
console.log(localStorage.getItem("movies"));

var movies = {
    all: function(){
        return JSON.parse( localStorage.getItem("movies") );
    },
    getMovie: function(title){
        var allMovies = this.all();
        var myMovie = allMovies[title];
        return title + ": director: "+myMovie.director+", year: "+myMovie.year;
    },
    setMovie: function(title, director, year){
        var allMovies = this.all();
        var data = {"director": director, "year": year};
        allMovies[title] = data;
        localStorage.setItem("movies", JSON.stringify(allMovies));
    },
    deleteMovie: function(title){
        var allMovies = this.all();
        delete allMovies[title];
        localStorage.setItem("movies", JSON.stringify(allMovies));
    }
};

var notify = function(title, content){
    if (window.webkitNotifications) {
        console.log("Notifications are supported!");
            if (window.webkitNotifications.checkPermission() === 0) {
            /* 0 is PERMISSION_ALLOWED */
            console.log("Notifications are supported!");
            window.webkitNotifications.requestPermission();
            window.webkitNotifications.createNotification('http://www.uma.es/publicadores/prevencion/wwwuma/sobre.gif', title, content).show();
        }else{
            console.log("Notifications are NOT supported!");
        }
    }else{
        console.log("Notifications are NOT supported!");
    }
    
    
};

notify('all the movies', JSON.stringify(movies.all()));
notify('movie established', movies.setMovie("Ironman", "rz", "2004"));
// console.log(JSON.stringify(movies.all()));
notify('movie deleted', movies.deleteMovie("Ironman"));
// console.log(JSON.stringify(movies.all()));

